INCLUDE C:/Irvine/Irvine32.inc
.data
	welcome1 byte "Welcome to Tower of Hanoi",0
	welcome2 byte "Enter how many discs you would like to play with: ",0

	numOfDiscs byte 0
	tempDisc byte ?
	currentDisc db ?

	leftstack byte 10 dup(0)
	middlestack byte 10 dup(0)
	rightstack byte 10 dup(0)

.code 

	main proc
		call WelcomeToGame

		exit
	main endp
	
	take proc ; This function takes the current disc off of the selected stack using eax to select
		.if currentDisc != 0 ; Not allowed to take if you already have a disc
			ret
		.endif		
		.if eax == 0 ; Take the top disc off the left stack and move it to the currentDisk
			mov edi, offset leftstack
			call myPop
		.elseif eax == 1 ; Move the current selected disc onto the middle stack
			mov edi, offset middlestack
			call myPop
		.elseif eax == 2 ; Move the current selected disc onto the right stack
			mov edi, offset rightstack
			call myPop
		.endif
		ret
	take endp

	place proc ; This function places the current disc onto the selected stack using eax to select
		.if currentDisc == 0 ; Not allowed to place if you have no disc (even though it would do nothing, just for sanity)
			ret
		.endif
		.if eax == 0 ; Move the current selected disc onto the left stack
			mov edi, offset leftstack
			call myPush
		.elseif eax == 1 ; Move the current selected disc onto the middle stack
			mov edi, offset middlestack
			call myPush
		.elseif eax == 2 ; Move the current selected disc onto the right stack
			mov edi, offset rightstack
			call myPush
		.endif
		ret
	place endp

	myPop proc uses edi ; takes the first non-zero value from the right and puts it into currentDisc, the array that it uses is in edi 
		mov ecx, 9
		L1:
			mov bl, [edi + ecx]
			movzx eax, bl
			cmp eax, 0
			jnz L2
		loop L1

			mov bl, [edi]
			movzx eax, bl
			call WriteInt
			call CRLF
		
		L2:
			add edi, ecx
			mov bl, 0
			mov [edi], bl
			mov currentDisc, al
		ret
	myPop endp

	myPush proc uses edi ; puts the value of currentDisc into the last zero value from the right, the array it uses is in edi
		mov ecx, 9
		L1:
			mov bl, [edi + ecx]
			movzx eax, bl
			cmp eax, 0
			jnz L2
		loop L1

		L2:
			add edi, ecx
			inc edi
			movzx edx, currentDisc
			mov [edi], edx
			mov currentDisc, 00
		ret
	myPush endp 

	myPeek proc uses edi ; puts the value of the first non-zero value in the array held in edi into tempDisc
		mov ecx, 9
		L1:
			mov bl, [edi + ecx]
			movzx eax, bl
			cmp eax, 0
			jnz L2
		loop L1

		L2:
			mov tempDisc, al
		ret
	myPeek endp
	
	WelcomeToGame proc ; Splash screen
		mov edx,offset welcome1
		call writestring
		call crlf
		mov edx,offset welcome2
		call writestring
		call readint
		mov numOfDiscs,al

		mov ecx, 0	
		mov esi, 0
		mov cl, numOfDiscs
		mov esi, offset leftstack

		Looper:
			mov [esi], cl
			dec bl
			inc esi
		Loop Looper
		

		ret
	WelcomeToGame endp

end main 